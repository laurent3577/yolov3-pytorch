from __future__ import division

import torch
import torch.nn as nn
import torch.nn.functional as F 
from torch.autograd import Variable
import numpy as np
import cv2
import pickle as pkl



class EmptyLayer(nn.Module):
	def __init__(self):
		super(EmptyLayer, self).__init__()

class DetectionLayer(nn.Module):
	def __init__(self, anchors):
		super(DetectionLayer, self).__init__()
		self.anchors = anchors

def parse_cfg(filepath):
	file = open(filepath, 'r')
	lines = file.read().split('\n')                        # store the lines in a list
	lines = [x for x in lines if len(x) > 0]               # get read of the empty lines 
	lines = [x for x in lines if x[0] != '#']              # get rid of comments
	lines = [x.rstrip().lstrip() for x in lines]           # get rid of fringe whitespaces

	block = {}
	blocks = []

	for line in lines:
		if line[0] == "[":               # This marks the start of a new block
			if len(block) != 0:          # If block is not empty, implies it is storing values of previous block.
				blocks.append(block)     # add it the blocks list
				block = {}               # re-init the block
			block["type"] = line[1:-1].rstrip()     
		else:
			key,value = line.split("=") 
			block[key.rstrip()] = value.lstrip()
	blocks.append(block)
	return blocks[0], blocks[1:]

def build_modules(blocks):
	module_list = nn.ModuleList()
	prev_filters = 3
	output_filters = []

	for index, x in enumerate(blocks):
		module = nn.Sequential()

		if x['type'] == 'convolutional':
			activation = x['activation']
			filters= int(x["filters"])
			padding = int(x["pad"])
			kernel_size = int(x["size"])
			stride = int(x["stride"])

			try:
				batch_normalize = int(x['batch_normalize'])
				bias = False
			except:
				batch_normalize = 0
				bias = True

			if padding:
				pad = (kernel_size - 1) // 2
			else:
				pad = 0

			#Add the convolutional layer
			conv = nn.Conv2d(prev_filters, filters, kernel_size, stride, pad, bias = bias)
			module.add_module("conv_{0}".format(index), conv)

			#Add the Batch Norm Layer
			if batch_normalize:
				bn = nn.BatchNorm2d(filters)
				module.add_module("batch_norm_{0}".format(index), bn)

			#Check the activation. 
			#It is either Linear or a Leaky ReLU for YOLO
			if activation == "leaky":
				activn = nn.LeakyReLU(0.1, inplace = True)
				module.add_module("leaky_{0}".format(index), activn)

		elif x["type"] == "upsample":
			stride = int(x["stride"])
			upsample = nn.Upsample(scale_factor = stride, mode = "bilinear", align_corners=False)
			module.add_module("upsample_{}".format(index), upsample)		   

		elif (x["type"] == "route"):
			x["layers"] = x["layers"].split(',')
			#Start  of a route
			start = int(x["layers"][0])
			#end, if there exists one.
			try:
				end = int(x["layers"][1])
			except:
				end = 0
			#Positive anotation
			if start > 0: 
				start = start - index
			if end > 0:
				end = end - index
			route = EmptyLayer()
			module.add_module("route_{0}".format(index), route)
			if end < 0:
				filters = output_filters[index + start] + output_filters[index + end]
			else:
				filters= output_filters[index + start]

		#shortcut corresponds to skip connection
		elif x["type"] == "shortcut":
			shortcut = EmptyLayer()
			module.add_module("shortcut_{}".format(index), shortcut)


		elif x["type"] == "yolo":
			mask = x["mask"].split(",")
			mask = [int(m) for m in mask]

			anchors = x["anchors"].split(",")
			anchors = [int(a) for a in anchors]
			anchors = [(anchors[i], anchors[i+1]) for i in range(0, len(anchors),2)]
			anchors = [anchors[i] for i in mask]

			detection = DetectionLayer(anchors)
			module.add_module("Detection_{}".format(index), detection)

		module_list.append(module)
		prev_filters = filters
		output_filters.append(filters)
	return module_list

def load_classes(filename):
	fp = open(filename, 'r')
	names = fp.read().split("\n")[:-1]
	return names 

def unique(tensor):
	tensor_np = tensor.numpy()
	unique_np = np.unique(tensor_np)
	return torch.FloatTensor(unique_np.copy())

def bbox_iou(box1, box2):
	 #Get the coordinates of bounding boxes
	b1_x1, b1_y1, b1_x2, b1_y2 = box1[:,0], box1[:,1], box1[:,2], box1[:,3]
	b2_x1, b2_y1, b2_x2, b2_y2 = box2[:,0], box2[:,1], box2[:,2], box2[:,3]
	
	#get the corrdinates of the intersection rectangle
	inter_rect_x1 =  torch.max(b1_x1, b2_x1)
	inter_rect_y1 =  torch.max(b1_y1, b2_y1)
	inter_rect_x2 =  torch.min(b1_x2, b2_x2)
	inter_rect_y2 =  torch.min(b1_y2, b2_y2)
	
	#Intersection area
	inter_area = torch.clamp(inter_rect_x2 - inter_rect_x1 + 1, min=0) * torch.clamp(inter_rect_y2 - inter_rect_y1 + 1, min=0)
 
	#Union Area
	b1_area = (b1_x2 - b1_x1 + 1)*(b1_y2 - b1_y1 + 1)
	b2_area = (b2_x2 - b2_x1 + 1)*(b2_y2 - b2_y1 + 1)
	
	iou = inter_area / (b1_area + b2_area - inter_area)
	
	return iou


def transform_predict(prediction, inp_dim, anchors, num_classes):
	batch_size = prediction.size(0)
	stride = inp_dim // prediction.size(2)
	grid_size = inp_dim // stride
	bbox_attrs = 5 + num_classes
	num_anchors = len(anchors)

	prediction = prediction.view(batch_size, bbox_attrs*num_anchors, grid_size*grid_size)
	prediction = prediction.transpose(1,2).contiguous()
	prediction = prediction.view(batch_size, grid_size*grid_size*num_anchors, bbox_attrs)

	anchors = [(a[0]/stride, a[1]/stride) for a in anchors]

	prediction[:,:,0] = torch.sigmoid(prediction[:,:,0])
	prediction[:,:,1] = torch.sigmoid(prediction[:,:,1])
	prediction[:,:,4] = torch.sigmoid(prediction[:,:,4])

	grid = np.arange(grid_size)
	a, b = np.meshgrid(grid, grid)

	x_offset = torch.FloatTensor(a).view(-1,1)
	y_offset = torch.FloatTensor(b).view(-1,1)

	x_y_offset = torch.cat((x_offset, y_offset),1).repeat(1,num_anchors).view(-1,2).unsqueeze(0)
	prediction[:,:,:2] += x_y_offset


	anchors = torch.FloatTensor(anchors)

	anchors = anchors.repeat(grid_size*grid_size,1).unsqueeze(0)
	prediction[:,:,2:4] = torch.exp(prediction[:,:,2:4])*anchors

	prediction[:,:,5:5+num_classes] = torch.sigmoid(prediction[:,:,5:5+num_classes])

	prediction[:,:,:4] *= stride

	return prediction

def filter_predict(prediction, confidence, num_classes, nms_conf =0.4):
	conf_mask = (prediction[:,:,4] > confidence).float().unsqueeze(2)
	prediction = prediction * conf_mask

	box_corner = prediction.new(prediction.shape)
	box_corner[:,:,0] = (prediction[:,:,0] - prediction[:,:,2]/2)
	box_corner[:,:,1] = (prediction[:,:,1] - prediction[:,:,3]/2)
	box_corner[:,:,2] = (prediction[:,:,0] + prediction[:,:,2]/2) 
	box_corner[:,:,3] = (prediction[:,:,1] + prediction[:,:,3]/2)
	prediction[:,:,:4] = box_corner[:,:,:4]


	batch_size = prediction.size(0)
	init = False
	output = None
	for ind in range(batch_size):
		image_pred = prediction[ind]
		print "Predictions on image : ", image_pred.shape[0]
		max_conf, max_conf_score = torch.max(image_pred[:,5:5+num_classes], 1)
		max_conf = max_conf.float().unsqueeze(1)
		max_conf_score = max_conf_score.float().unsqueeze(1)

		seq = (image_pred[:,:5], max_conf, max_conf_score)
		image_pred = torch.cat(seq,1)
		
		non_zero_ind = torch.nonzero(image_pred[:,4]).squeeze()
		image_pred_ = image_pred[non_zero_ind].view(-1,7)
		print "After confidence filtering : ", image_pred_.shape[0]
		if image_pred_.shape[0]==0:
			continue

		img_classes = unique(image_pred_[:,-1])
		for cl in img_classes:
			cl_mask = image_pred_*(image_pred_[:,-1]==cl).float().unsqueeze(1)
			class_mask_ind = torch.nonzero(cl_mask[:,-2]).squeeze()
			image_pred_class = image_pred_[class_mask_ind].view(-1,7)

			conf_sort_index = torch.sort(image_pred_class[:,4], descending=True)[1]
			image_pred_class = image_pred_class[conf_sort_index]
			idx = image_pred_class.size(0)

			for i in range(idx):
				try:
					ious = bbox_iou(image_pred_class[i].unsqueeze(0), image_pred_class[i+1:])
				except ValueError:
					break
				except IndexError:
					break

				iou_mask = (ious < nms_conf).float().unsqueeze(1)
				
				image_pred_class[i+1:] *= iou_mask

				non_zero_ind = torch.nonzero(image_pred_class[:,4]).squeeze()
				image_pred_class = image_pred_class[non_zero_ind].view(-1,7)

			batch_ind = image_pred_class.new(image_pred_class.size(0),1).fill_(ind)
			seq = batch_ind, image_pred_class

			if not init:
				output = torch.cat(seq,1)
				init = True
			else:
				out = torch.cat(seq,1)
				output = torch.cat((output,out))
		print('After Non Maximum supression : {}'.format(output.shape[0]))
	return output

def draw(x, img):
	color = (np.random.randint(0,256),np.random.randint(0,256),np.random.randint(0,256))
	c1 = tuple(x[1:3].int())
	c2 = tuple(x[3:5].int())
	cl = int(x[-1])
	label = "{}".format(classes[cl])
	cv2.rectangle(img,c1,c2,color,1)
	t_size = cv2.getTextSize(label, cv2.FONT_HERSHEY_PLAIN,1,1)[0]
	c2 = c1[0] + t_size[0] + 3, c1[1] + t_size[1] + 4
	cv2.rectangle(img, c1, c2, color, -1)
	cv2.putText(img, label, (c1[0], c1[1] + t_size[1] + 4), cv2.FONT_HERSHEY_PLAIN, 1, [255,255,255],1)
	return img

classes = load_classes('coco.names.txt')

