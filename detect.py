from darknet import *
from utils import *
from collections import Counter
import argparse
import time 


if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument("-i", help='Image file to detect', type=str)
	parser.add_argument("-c", help='Confidence threshold for detection', type=float)
	parser.add_argument("-f", help='Non maximum suppression filtering threshold', type=float)
	parser.set_defaults(c=0.3)
	parser.set_defaults(f=0.4)

	args = parser.parse_args()
	img_path = args.i
	c = float(args.c)
	f = float(args.f)

	print('Loading pretrained model...')
	model = YOLO('cfg/yolov3.cfg')
	print('Model loaded')
	print('Detecting...')
	start = time.time()
	prediction = model.detect(img_path,confidence=c, nms_conf=f)
	end = time.time()
	classes = load_classes('coco.names.txt')
	objs = Counter([classes[int(c)] for c in prediction[:,-1]])
	print('Detection completed')
	print("----------------------------------------------------------")
	print('			SUMMARY				')
	print("\n{0:20s} predicted in {1:.3f} seconds".format(img_path.split("/")[-1], (end - start)))
	print('\nObjects detected')
	for obj, nb in objs.items():
		print('{0:20s} : {1:6}'.format(obj, nb))
	print("----------------------------------------------------------")

	exit()