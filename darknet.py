from __future__ import division

import torch 
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
import cv2 
from torch.autograd import Variable
from utils import *


class YOLO(nn.Module):
	def __init__(self, cfgfile, load_weights=True):
		super(YOLO, self).__init__()
		self.net_info, self.blocks = parse_cfg(cfgfile)
		self.module_list = build_modules(self.blocks)
		if load_weights:
			self.load_weights()

	def load_weights(self):
		fp = open('yolov3.weights', 'rb')
		header = np.fromfile(fp, dtype=np.int32, count=5)
		self.header = torch.from_numpy(header)
		self.seen = self.header[3]

		weights = np.fromfile(fp, dtype=np.float32)
		ptr = 0
		for i in range(len(self.module_list)):
			module_type= self.blocks[i]["type"]

			if module_type == 'convolutional':
				model = self.module_list[i]
				try:
					batch_normalize = int(self.blocks[i]["batch_normalize"])
				except:
					batch_normalize = 0

				conv = model[0]
				if batch_normalize:
					bn = model[1]

					num_bn_biases = bn.bias.numel()
					bn_biases = torch.from_numpy(weights[ptr:ptr+num_bn_biases])
					ptr += num_bn_biases

					bn_weights = torch.from_numpy(weights[ptr:ptr+num_bn_biases])
					ptr += num_bn_biases

					bn_running_mean = torch.from_numpy(weights[ptr:ptr+num_bn_biases])
					ptr += num_bn_biases

					bn_running_var = torch.from_numpy(weights[ptr:ptr+num_bn_biases])
					ptr += num_bn_biases

					#Cast the loaded weights into dims of model weights. 
					bn_biases = bn_biases.view_as(bn.bias.data)
					bn_weights = bn_weights.view_as(bn.weight.data)
					bn_running_mean = bn_running_mean.view_as(bn.running_mean)
					bn_running_var = bn_running_var.view_as(bn.running_var)

					#Copy the data to model
					bn.bias.data.copy_(bn_biases)
					bn.weight.data.copy_(bn_weights)
					bn.running_mean.copy_(bn_running_mean)
					bn.running_var.copy_(bn_running_var)

				else:
					num_biases = conv.bias.numel()

					conv_biases = torch.from_numpy(weights[ptr:ptr+num_biases])
					ptr += num_biases

					conv_biases = conv_biases.view_as(conv.bias.data)
					conv.bias.data.copy_(conv_biases)

				num_weights = conv.weight.numel()

				conv_weights = torch.from_numpy(weights[ptr:ptr+num_weights])
				ptr += num_weights

				conv_weights = conv_weights.view_as(conv.weight.data)
				conv.weight.data.copy_(conv_weights)


	def forward(self, x):
		modules = self.blocks
		outputs = {}
		init = False
		for i, module in enumerate(modules):
			module_type = module['type']
			if module_type in ["convolutional", "upsample"]:
				x = self.module_list[i](x)

			elif module_type == "route":
				layers = module["layers"]
				layers = [int(l) for l in layers]

				if layers[0]>0:
					layers[0] = layers[0] - i

				if len(layers) == 1:
					x = outputs[i + layers[0]]

				else:
					if layers[1]>0:
						layers[1] = layers[1] - i 

					map1 = outputs[i+layers[0]]
					map2 = outputs[i+layers[1]]

					x = torch.cat((map1,map2),1)

			elif module_type == "shortcut":
				from_ = int(module["from"])
				x = outputs[i-1] + outputs[i+from_]

			elif module_type == 'yolo':

				anchors = self.module_list[i][0].anchors
				inp_dim = int(self.net_info['height'])
				num_classes = int(module['classes'])

				x = x.data
				x = transform_predict(x, inp_dim, anchors, num_classes)
				if not init:
					detections = x
					init = True
				else:
					detections = torch.cat((detections,x),1)

			outputs[i] = x

		return detections


	def load_image(self, img_path):
		img = cv2.imread(img_path)
		w_, h_ = img.shape[1], img.shape[0]
		w, h = int(self.net_info['width']), int(self.net_info['height'])
		r =  min(w/w_,h/h_)
		new_w = int(w_ * r)
		new_h = int(h_ * r)
		resized_image = cv2.resize(img, (new_w,new_h), interpolation=cv2.INTER_CUBIC)

		img = np.full((h,w,3),128)
		img[(h-new_h)//2:(h-new_h)//2 + new_h, (w-new_w)//2:(w-new_w)//2 + new_w, :] = resized_image
		
		img = img[:,:,::-1].transpose((2,0,1)).copy()
		img = torch.FloatTensor(img).div(255.0).unsqueeze(0)
		
		return img, w_, h_ 


	def detect(self, img_path, confidence=0.3, num_classes=80, nms_conf=0.4):
		img, W, H = self.load_image(img_path)
		prediction = self.forward(Variable(img, requires_grad=False))
		prediction = filter_predict(prediction, confidence, num_classes, nms_conf)

		if prediction is None:
			print 'No detection made'
			return
		else:
			inp_dim = int(self.net_info['height'])
			
			orig_dim = torch.FloatTensor([W,H]).repeat(1,2)
			scaling_factor = torch.min(inp_dim/orig_dim,1)[0].view(-1,1)
			
			prediction[:, [1,3]] -= (inp_dim - scaling_factor*W)/2
			prediction[:, [2,4]] -= (inp_dim - scaling_factor*H)/2
			prediction[:,1:5] *= 1./scaling_factor

			for i in range(prediction.shape[0]):
				prediction[i, [1,3]] = torch.clamp(prediction[i, [1,3]], 0, W)
				prediction[i, [2,4]] = torch.clamp(prediction[i, [2,4]], 0, H)
			original_im = cv2.imread('images/'+img_path)
			list(map(lambda x: draw(x,original_im), prediction))
			cv2.imwrite('images/det_'+img_path, original_im)
		return prediction

			